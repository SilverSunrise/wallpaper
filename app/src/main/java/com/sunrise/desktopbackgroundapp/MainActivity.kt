package com.sunrise.desktopbackgroundapp

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var adapters: Adapters

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        adapter()
    }

    private fun adapter() {
        val list = imageList()
        adapters = Adapters(this)
        adapters.setContentList(list)
        vp_slider.adapter = adapters
    }

    private fun imageList(): MutableList<Int> {
        val list = mutableListOf<Int>()
        list.add(R.drawable.first)
        list.add(R.drawable.second)
        list.add(R.drawable.third)
        list.add(R.drawable.fourth)
        list.add(R.drawable.five)
        list.add(R.drawable.six)
        list.add(R.drawable.seven)
        list.add(R.drawable.eight)
        return list
    }
}


