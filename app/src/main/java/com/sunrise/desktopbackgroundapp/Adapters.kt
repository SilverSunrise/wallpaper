package com.sunrise.desktopbackgroundapp

import android.annotation.SuppressLint
import android.app.WallpaperManager
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView

class Adapters(private val context: Context) : RecyclerView.Adapter<Adapters.MyViewHolder>() {

    lateinit var list: List<Int>

    fun setContentList(list: List<Int>) {
        this.list = list
        notifyDataSetChanged()
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var image: ImageView = itemView.findViewById(R.id.list_item_image)
        var btnSetWallpaper: Button = itemView.findViewById(R.id.btn_set_wallpaper)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Adapters.MyViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.list_wallpaper, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("ResourceType")
    override fun onBindViewHolder(holder: Adapters.MyViewHolder, position: Int) {
        holder.image.setImageResource(list[position])
        holder.btnSetWallpaper.setOnClickListener {
            val wallpaperManager: WallpaperManager = WallpaperManager.getInstance(context)
            when (list[position]) {
                list[0] -> wallpaperManager.setResource(R.drawable.first)
                list[1] -> wallpaperManager.setResource(R.drawable.second)
                list[2] -> wallpaperManager.setResource(R.drawable.third)
                list[3] -> wallpaperManager.setResource(R.drawable.fourth)
                list[4] -> wallpaperManager.setResource(R.drawable.five)
                list[5] -> wallpaperManager.setResource(R.drawable.six)
                list[6] -> wallpaperManager.setResource(R.drawable.seven)
                list[7] -> wallpaperManager.setResource(R.drawable.eight)

            }
        }

    }
}